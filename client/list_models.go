package mlaasserver

import (
	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

// ListModels invokes the GRPC operations to list models for a user
func ListModels(userID string) ([]string, error) {

	ctx := context.Background()

	// Create a root span for this operation
	sp, ctx := tracing.StartSpanFromContext(ctx, "listmodels")
	defer sp.Finish()

	conn, client := MustGetServerClient(ctx)
	defer conn.Close()

	req := &services.ListModelsRequest{Userid: userID}

	repl, err := client.ListModels(ctx, req)
	if err != nil {
		log.WithError(err).Fatal()
	}
	return repl.GetStrings(), nil
}
