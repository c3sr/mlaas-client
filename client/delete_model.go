package mlaasserver

import (
	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

// ListModels invokes the GRPC operations to list models for a user
func DeleteModel(userID, modelID string) (string, error) {
	_, closer := tracing.Globals()
	defer closer.Close()

	ctx := context.Background()

	// Create a root span for this operation
	sp, ctx := tracing.StartSpanFromContext(ctx, "deletemodel")
	defer sp.Finish()

	conn, client := MustGetServerClient(ctx)
	defer conn.Close()

	req := &services.ModelRequest{Userid: userID, Modelid: modelID}

	repl, err := client.DeleteModel(ctx, req)
	if err != nil {
		log.WithError(err).Fatal()
	}
	return repl.GetMessage(), nil
}
