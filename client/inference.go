package mlaasserver

import (
	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

func Inference(ctx context.Context, data []byte, userID, modelID string) (string, error) {

	sp, ctx := tracing.StartSpanFromContext(ctx, "inference")
	defer sp.Finish()

	// Create an inference request
	req := &services.ClientInferenceRequest{
		Userid:  userID,
		Modelid: modelID,
		Data:    data,
	}

	conn, serverClient := MustGetServerClient(ctx)
	defer conn.Close()

	resp, err := serverClient.Inference(ctx, req)
	if nil != err {
		log.WithError(err).Fatal("Got error response from mlaas-server")
	}

	return resp.Class, nil
}
