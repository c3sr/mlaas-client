package mlaasserver

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	etcd "bitbucket.org/c3sr/p3sr-etcd-client"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

func MustFindServer(c context.Context) string {
	sp, _ := tracing.StartSpanFromContext(c, "etcd_lookup_server")
	defer sp.Finish()
	addr, err := etcd.FindService(services.ServerServiceDescription)
	if err != nil {
		log.WithError(err).Fatal("couldn't find mlaas-server service. Is it running?")
	}
	return addr
}

func MustDialServer(c context.Context, addr string) *grpc.ClientConn {
	sp, _ := tracing.StartSpanFromContext(c, "dial_server")
	defer sp.Finish()
	serverConn, err := pg.Dial(services.ServerServiceDescription, addr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Fatal("Unable to dial mlaas-server service at ", addr)
	}
	return serverConn
}

func MustGetServerClient(c context.Context) (*grpc.ClientConn, services.ServerServiceClient) {
	// Look up the MLaaS server
	addr := MustFindServer(c)
	conn := MustDialServer(c, addr)

	// Connect to the server
	serverClient := services.NewServerServiceClient(conn)

	return conn, serverClient
}
