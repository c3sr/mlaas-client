package mlaasserver

import (
	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

func StoreModel(ctx context.Context, userid string, pb, ckpt, meta []byte) (string, error) {
	// Create a span for this operation
	sp, ctx := tracing.StartSpanFromContext(ctx, "client_store_model")
	defer sp.Finish()

	conn, client := MustGetServerClient(ctx)
	defer conn.Close()

	model := &services.TfModel{Pb: pb, Ckpt: ckpt, Ckptmeta: meta}
	req := &services.StoreModelRequest{Userid: userid, Model: model}

	repl, err := client.StoreModel(ctx, req)
	if err != nil {
		log.WithError(err).Fatal()
	}
	return repl.GetModelid(), nil
}
