package cmd

import (
	"fmt"

	client "bitbucket.org/c3sr/mlaas-client/client"
	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
)

var listModelsCmd = &cobra.Command{
	Use:   "list-models",
	Short: "List models for a user",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
		if userID == "" {
			cmd.Help()
			return
		}

		models, err := client.ListModels(userID)
		if err != nil {
			log.WithError(err).Fatal()
		}
		for _, model := range models {
			fmt.Println(model)
		}
	},
}

func init() {
	listModelsCmd.Flags().StringVarP(&userID, "user", "u", "", "user id")
	RootCmd.AddCommand(listModelsCmd)
}
