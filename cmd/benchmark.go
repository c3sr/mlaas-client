package cmd

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strconv"
	"sync"

	client "bitbucket.org/c3sr/mlaas-client/client"
	log "bitbucket.org/c3sr/p3sr-logger"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	"github.com/spf13/cobra"
)

// titleCmd represents the title command
var benchmarkCmd = &cobra.Command{
	Use:   "benchmark",
	Short: "benchmark inference",
	Run: func(cmd *cobra.Command, args []string) {
		if pbPath == "" || ckptPath == "" || metaPath == "" || userID == "" {
			cmd.Help()
			return
		}

		// Create a new named tracer for this operation
		tracer, closer := tracing.New("mlaas-client-benchmark")
		defer closer.Close()

		// Configure p3sr-trace to use it for spans that come from here
		tracing.InitGlobalTracer(tracer)

		// Read the model
		pb, err := ioutil.ReadFile(pbPath)
		if err != nil {
			log.WithError(err).Fatal()
		}
		ckpt, err := ioutil.ReadFile(ckptPath)
		if err != nil {
			log.WithError(err).Fatal()
		}
		meta, err := ioutil.ReadFile(metaPath)
		if err != nil {
			log.WithError(err).Fatal()
		}

		// Create a root span for this benchmark
		ctx := context.Background()
		sp, ctx := tracing.StartSpanFromContext(ctx, "benchmark")
		defer sp.Finish()

		// Create ten new models
		numModels := 10
		var wg sync.WaitGroup
		wg.Add(numModels)
		for i := 0; i < numModels; i++ {

			go func() {
				defer wg.Done()
				model, err := client.StoreModel(ctx, userID, pb, ckpt, meta)
				if err != nil {
					log.WithError(err).Error("Unable to store model")
				}
				fmt.Println(model)
			}()
		}
		wg.Wait()

		// Read inference data
		data, err := ioutil.ReadFile(dataPath)
		if err != nil {
			log.WithError(err).Fatal()
		}

		// Issue inference commands on those models
		numInferences := 10
		wg.Add(numInferences)
		for i := 0; i < numInferences; i++ {
			go func() {
				defer wg.Done()
				class, err := client.Inference(ctx, data, userID, "model"+strconv.Itoa(rand.Int()%numModels))
				fmt.Println(class, err)
			}()
		}
		wg.Wait()
	},
}

func init() {
	benchmarkCmd.Flags().StringVarP(&userID, "user", "u", "", "user id")
	benchmarkCmd.Flags().StringVarP(&pbPath, "pb", "p", "", "path to pb file")
	benchmarkCmd.Flags().StringVarP(&ckptPath, "ckpt", "c", "", "path to ckpt file")
	benchmarkCmd.Flags().StringVarP(&metaPath, "meta", "m", "", "path to ckpt meta file")
	benchmarkCmd.Flags().StringVarP(&dataPath, "data", "", "", "path to inference data")

	RootCmd.AddCommand(benchmarkCmd)
}
