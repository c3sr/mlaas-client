package cmd

import (
	"context"
	"io/ioutil"

	client "bitbucket.org/c3sr/mlaas-client/client"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
)

var userID, pbPath, ckptPath, metaPath string
var storeModelCmd = &cobra.Command{
	Use:   "store-model",
	Short: "Store a set of model files",
	Run: func(cmd *cobra.Command, args []string) {
		if pbPath == "" || ckptPath == "" || metaPath == "" || userID == "" {
			cmd.Help()
			return
		}

		// Create a new named tracer for this operation
		// Configure p3sr-trace to use it for spans that come from here
		tracer, closer := tracing.New("mlaas-client-store-model")
		defer closer.Close()
		tracing.InitGlobalTracer(tracer)

		// Create a root span for this benchmark
		ctx := context.Background()
		sp, ctx := tracing.StartSpanFromContext(ctx, "store_model_root")
		defer sp.Finish()

		readSp, _ := tracing.StartSpanFromContext(ctx, "read_files")
		pb, err := ioutil.ReadFile(pbPath)
		if err != nil {
			log.WithError(err).Fatal()
		}
		ckpt, err := ioutil.ReadFile(ckptPath)
		if err != nil {
			log.WithError(err).Fatal()
		}
		meta, err := ioutil.ReadFile(metaPath)
		if err != nil {
			log.WithError(err).Fatal()
		}
		readSp.Finish()

		modelID, err := client.StoreModel(ctx, userID, pb, ckpt, meta)
		if err != nil {
			log.WithError(err).Fatal()
		}
		log.Info("Model id:\n", modelID)
		return
	},
}

func init() {

	storeModelCmd.Flags().StringVarP(&userID, "user", "u", "", "user id")
	storeModelCmd.Flags().StringVarP(&pbPath, "pb", "p", "", "path to pb file")
	storeModelCmd.Flags().StringVarP(&ckptPath, "ckpt", "c", "", "path to ckpt file")
	storeModelCmd.Flags().StringVarP(&metaPath, "meta", "m", "", "path to ckpt meta file")

	RootCmd.AddCommand(storeModelCmd)
}
