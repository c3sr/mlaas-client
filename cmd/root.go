package cmd

import (
	"fmt"
	"os"

	config "bitbucket.org/c3sr/p3sr-config"
	"github.com/spf13/cobra"
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "mlaas-client",
	Short: "A simple MLaaS client for IE 598",
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	config.Init()
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {

	RootCmd.PersistentFlags().BoolVarP(&config.App.Verbose, "verbose", "v", config.App.Verbose, "verbose output")
	RootCmd.PersistentFlags().BoolVarP(&config.App.Debug, "debug", "d", config.App.Debug, "debout output")
}
