package cmd

import (
	client "bitbucket.org/c3sr/mlaas-client/client"
	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
)

var modelID string
var deleteModelCmd = &cobra.Command{
	Use:   "delete-model",
	Short: "Delete a model for a user",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
		if userID == "" || modelID == "" {
			cmd.Help()
			return
		}
		_, err := client.DeleteModel(userID, modelID)
		if err != nil {
			log.WithError(err).Fatal()
		}
	},
}

func init() {
	deleteModelCmd.Flags().StringVarP(&userID, "user", "u", "", "user id")
	deleteModelCmd.Flags().StringVarP(&modelID, "model", "m", "", "model id")
	RootCmd.AddCommand(deleteModelCmd)
}
