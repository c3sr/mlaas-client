package cmd

import (
	log "bitbucket.org/c3sr/p3sr-logger"

	"github.com/spf13/cobra"
)

// trainCmd represents the train command
var trainCmd = &cobra.Command{
	Use:   "train",
	Short: "Train a model on some data",
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
		log.Info("mlaas-client train called")
	},
}

func init() {
	RootCmd.AddCommand(trainCmd)
}
