package cmd

import (
	"context"
	"io/ioutil"

	client "bitbucket.org/c3sr/mlaas-client/client"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
)

var dataPath string

// titleCmd represents the inferencecommand
var inferenceCmd = &cobra.Command{
	Use:   "inference",
	Short: "Do inference on some data",
	Run: func(cmd *cobra.Command, args []string) {

		if dataPath == "" || modelID == "" || userID == "" {
			cmd.Help()
			return
		}

		// Create a new named tracer for this operation
		// Configure p3sr-trace to use it for spans that come from here
		tracer, closer := tracing.New("mlaas-client-inference")
		defer closer.Close()
		tracing.InitGlobalTracer(tracer)

		// Create a root span for this command
		ctx := context.Background()
		sp, ctx := tracing.StartSpanFromContext(ctx, "inference_root")
		defer sp.Finish()

		readSp, _ := tracing.StartSpanFromContext(ctx, "read_files")
		data, err := ioutil.ReadFile(dataPath)
		readSp.Finish()
		if err != nil {
			log.WithError(err).Fatal()
		}

		class, err := client.Inference(ctx, data, userID, modelID)
		if err != nil {
			log.WithError(err).Fatal()
		}
		log.Info("Class : ", class)
		return
	},
}

func init() {

	inferenceCmd.Flags().StringVarP(&dataPath, "data", "", "", "path to the inference data")
	inferenceCmd.Flags().StringVarP(&modelID, "model", "", "", "the model id")
	inferenceCmd.Flags().StringVarP(&userID, "user", "u", "", "the user id")

	RootCmd.AddCommand(inferenceCmd)
}
